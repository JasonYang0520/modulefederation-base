import { Component } from '@angular/core';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'base';
  eventBusData = '';

  constructor() {
    fromEvent<CustomEvent>(window, 'app-event-bus').subscribe((e) => {
      this.onEventHandler(e);
    });
  }

  onEventHandler(e: CustomEvent) {
    this.eventBusData = e.detail.customData;

  }
}
