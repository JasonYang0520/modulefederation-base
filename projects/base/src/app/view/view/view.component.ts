import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SharedLibService } from 'shared-lib';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit, OnDestroy {

  data = '';
  sharedLibDataSubscription = new Subscription();
  constructor(private sharedLibService: SharedLibService) {
    this.sharedLibDataSubscription = this.sharedLibService.data.subscribe(val => {
      console.log('subscribe => ', val);
      this.data = val;
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.sharedLibDataSubscription.unsubscribe();
  }

}
