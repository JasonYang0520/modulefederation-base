import { MicroFrontendService } from './core/services/micro-frontend.service';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewModule } from './view/view.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ViewModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (microFrontendService: MicroFrontendService) => () => microFrontendService.getAppRoutes().toPromise(),
      deps: [MicroFrontendService,],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
