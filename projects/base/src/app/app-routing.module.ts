import { loadRemoteModule } from '@angular-architects/module-federation';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const url = 'http://localhost:3000/remoteEntry.js';

export const routes: Routes = [
  {
    path: 'view',
    loadChildren: () => import('./view/view.module').then(mod => mod.ViewModule)
  },
  // {
  //   path: 'query',
  //   loadChildren: () =>
  //     loadRemoteModule({
  //       // type: 'module',
  //       // remoteEntry: url,
  //       type: 'manifest',
  //       remoteName: 'app1',
  //       exposedModule: 'QueryModule',
  //     }).then((m) => m.QueryModule),
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
