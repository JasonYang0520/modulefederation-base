import { loadRemoteModule } from '@angular-architects/module-federation';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Routes, Router } from '@angular/router';
import { tap } from 'rxjs';
import { routes } from '../../app-routing.module';
import { CustomRemoteConfig } from '../interfaces/RemoteConfig';

@Injectable({
  providedIn: 'root'
})
export class MicroFrontendService {

  constructor(private http: HttpClient, private router: Router) { }

  getAppRoutes() {
    return this.http.get('http://localhost:4444/assets/test.json').pipe(
      tap((val: any) => {
        const lazyRoutes: Routes = Object.keys(val).map(key => {
          const entry: CustomRemoteConfig = val[key];
          return {
            path: entry.path,
            loadChildren: () => loadRemoteModule({
              type: 'module',
              remoteEntry: entry.url,
              exposedModule: entry.exposedModule
            }).then(m => m[entry.ngModuleName])
          };
        });

        const newRoutes = [...routes, ...lazyRoutes];
        this.router.resetConfig(newRoutes);
      })
    );
  }

}
