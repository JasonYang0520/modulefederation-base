
export interface CustomRemoteConfig {
  url: string;
  path: string;
  ngModuleName: string;
  exposedModule: string;
}
