## init
建立空的 angular application
```shell
ng new base --create-application=false
```

建立 base application
```shell
ng g application base --routing
```

加入 module-federation 套件
```shell
ng add @angular-architects/module-federation --project base --port 3000
```

## 引用遠端 application 方法1 - approuting 給 url

app-routing.module.ts import loadRemoteModule
```shell
import { loadRemoteModule } from '@angular-architects/module-federation';
```
<img src="./doc/approuting.png">

url => remote application 的url

exposedModule => remote expose 的 module name

- 透過 routerLink 即可導到 remote application



## 引用遠端 application 方法2 - 新增 json 及修改 main.ts
assets 下新增 mf.manifest.json 檔案，內容如下
```shell
{
	"app1": "http://localhost:3000/remoteEntry.js"
}
```


main.ts 加入 loadManifest 程式
```shell
import { loadManifest } from '@angular-architects/module-federation';

loadManifest("/assets/mf.manifest.json")
  .catch(err => console.error(err))
  .then(_ => import('./bootstrap'))
  .catch(err => console.error(err));
```
<img src="./doc/main.png">


app-routing.module.ts import loadRemoteModule
```shell
import { loadRemoteModule } from '@angular-architects/module-federation';
```
<img src="./doc/approuting2.png">

type => mainfest

remoteName => json 檔的 key

exposedModule => remote expose 的 module name

## 共用 library 資料
在 webpack.config.js 中 shared 加入要共用的 library，並設定 singleton: true
```shell
"package 名稱": { singleton: true }
```
- Singleton: 是否為單例，default 為 false
- strictVersion: 為 true 時，實際應用依賴的版本不一致時，會抛錯。
- requiredVersion: 指定共享依賴的版本，default 為當前的依賴版本
- eager: 在打包過程中是否被分離為 async chunk，為 true 時，會打包到 main、remoteEntry，不會被分離，因此設置為 true 時共享依賴是沒意義的。

## 透過 CustomEvent 傳遞資料
```shell
const data = {
  bubbles: true,
  detail: {
    eventType: 'test',
    customData: this.eventBusData
  }
};
const busEvent = new CustomEvent('app-event-bus', data);
dispatchEvent(busEvent);
```
new CustomEvent('app-event-bus', data)
- app-event-bus 為自定義的事件名稱
- data 需要交換的資料


## 透過 CustomEvent 接收資料
透過 rxjs 的fromEvent 訂閱 CustomEvent 事件
```shell
fromEvent<CustomEvent>(window, 'app-event-bus').subscribe((e) => {
  // do something
});
```
fromEvent<CustomEvent>(window, 'app-event-bus')
- app-event-bus 為自定義的事件名稱
- e 即為丟出來的資料

## 動態載入子應用 routing
AppRoutingModule routes 改成 export routes

新增 MicroFrontendService 及 getAppRoutes 方法
``` ts
getAppRoutes() {
    return this.http.get('http://localhost:4444/assets/test.json').pipe(
      tap((val: any) => {
        const lazyRoutes: Routes = Object.keys(val).map(key => {
          const entry: CustomRemoteConfig = val[key];
          return {
            path: entry.path,
            loadChildren: () => loadRemoteModule({
              type: 'module',
              remoteEntry: entry.url,
              exposedModule: entry.exposedModule
            }).then(m => {
              console.log('then');
              fromEvent<CustomEvent>(window, 'QueryModule').subscribe((e) => {
                // do something
                console.log('RegisterEventResolver',e.detail);
              });
              return m[item.ngModuleName];
            }).catch(reason => this.router.navigate(['/404']))
          };
        });

        const newRoutes = [...routes, ...lazyRoutes];
        this.router.resetConfig(newRoutes);
      })
    );
  }
```
- loadChildren .then 後需要加 .catch，避免錯誤造成白畫面
- 需要監聽子應用的 customEvent 的話可以寫在 .then 裡面註冊
  - 當 module 只有一個大家是透過該 module 的 lazy loading 載入時，可使用同樣的 customEvent 名稱並在裡面加 type，並把 event 放到 service 的 subject 中做 next，要使用的判斷 subject 的 type 來獲取自己想取用的事件資料。
  - 當 module 拆成多個時可以在各自裡面 .then 來監聽，或是把資料來源改成 ts 檔，並在裡面定義各 module 的 .then 需要執行的方法


AppModule providers 中增加 APP_INITIALIZER 的 provider 
```shell
    {
      provide: APP_INITIALIZER,
      useFactory: (microFrontendService: MicroFrontendService) => () => microFrontendService.getAppRoutes().toPromise(),
      deps: [MicroFrontendService,],
      multi: true
    }
```
- toPromise 確認 api 回來新增完 routing，避免在子應用的 routing 重新整理發生錯誤

## ts 檔中動態載入子應用
``` html
<div #placeHolder></div>
```
component.html 中定義標籤，該區塊為要載入子應用的 component 要放的位置

``` ts
  @ViewChild('placeHolder', { read: ViewContainerRef })
  viewContainer!: ViewContainerRef;

  async ngOnInit(): Promise<void> {
    await this.load();
  }
  
  async load(): Promise<void> {
    const m = await loadRemoteModule({
      type: 'module',
      remoteEntry: 'http://localhost:3000/remoteEntry.js',
      exposedModule: './Component'
    });

    const ref = this.viewContainer.createComponent<any>(m.QueryComponent);
    console.log(ref);
    
    // ref.instance['aaa'] = 'aaa';
    // const compInstance = ref.instance;
  }
```
component.ts 中透過 loadRemoteModule 載入子應用的 component，ref.instance 可直接取到該 component 的所有變數及方法  
###  **!!!** 假設該 component 有使用 ui 套件但是父應用沒有安裝的話會因為吃不到 style 的問題造成錯誤或跑版，子應用需要把 component 改用 shadow dom 的方式才會有 style，但假設是 overlay（dialog, dropdown）的話還需要在子應用額外寫 provider 才會正確...
https://medium.com/@epavliy/css-isolation-with-angular-micro-frontends-eb3bd8261bb9

<p>

## 使用 ng add 安裝套件
```shell
ng g @angular-architects/module-federation:boot-async false --project yourProject

ng add [libraries] --project yourProject

ng g @angular-architects/module-federation:boot-async true --project yourProject
```
